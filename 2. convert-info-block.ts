require('dotenv').config(); // Keep this at the top
import { bold, green, red, yellowBright } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';
import { html2richtext } from './src/html2richtext';
import { alertTemplate } from './templates/alert.template';
import { textBlokTemplate } from './templates/text.template';

const STORYBLOK_RELEASE = 150420;

type HtmlBlock = {
  _uid: string;
  code: {
    code: string;
    language: string;
  };
  component: 'knowledge_html';
};

type Section = {
  _uid: string;
  content: Array<HtmlBlock | any>;
  component: string;
  vertical_rhythm: string;
};

const program = new Command();
program
  .name('convert-info-boxes-to-bloks')
  .description(
    'Loops through all info alert boxes and converts them to actual alert boxes',
  )
  .option('-d, --dry-run', 'Run the script without actually coverting anything')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];
let allLinks: ISbStoryData[];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
      release_id: STORYBLOK_RELEASE,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {
        release_id: STORYBLOK_RELEASE,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

const getAlertContext = (html: string): { title: string; text: string } => {
  console.log(`Processing HTML: ${html}`);
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const title = document.querySelector('.title')?.textContent;
  const text = document.querySelector(
    '.confluence-information-macro-body',
  )?.innerHTML;

  return {
    title,
    text,
  };
};

async function findAndUpdateHtmlBlok(story: ISbStoryData): Promise<any> {
  const storyBody = story.content.body;
  return processSections(storyBody);
}

const processSections = (sections: Section[]): Section[] => {
  return sections.map((section) => {
    const newContent = section.content.flatMap((block) => {
      if (block.component === 'knowledge_html') {
        if (block.code.code.includes('confluence-information-macro-tip')) {
          console.log(`Found info section in ${section._uid}`);
          const alertContext = getAlertContext(block.code.code);

          const alertBlok = alertTemplate(
            'success',
            alertContext.title.trim(),
            html2richtext(alertContext.text),
          );

          return [textBlokTemplate('Links'), alertBlok];
        } else {
          // Return the block as is if it doesn't contain accordions
          return block;
        }
      } else {
        // Sections other than HTML blocks
        return block;
      }
    });

    return {
      ...section,
      content: newContent,
    };
  });
};

async function processArticles() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      !story.slug.includes('_common') &&
      !story.slug.includes('navigation')
    );
  });

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    if (
      !JSON.stringify(storyBody)?.includes('confluence-information-macro-tip')
    ) {
      console.log(`No information sections to uplift ${story.name}`);
      continue;
    } else {
      console.log(yellowBright`Found info section in ${story.name}`);
    }

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyContent);

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    console.log(green`Updating ${story.name}`);

    const updatedStory = {
      ...storyContent,
      content: {
        ...storyContent.content,
        body: updatedStoryBody,
      },
      publish: true,
      release_id: STORYBLOK_RELEASE,
    };

    if (!options.dryRun) {
      try {
        // console.log(JSON.stringify(updatedStory, null, 2));
        // Update the story with the new body
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          updatedStory,
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

processArticles();
