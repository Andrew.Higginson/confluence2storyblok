require('dotenv').config(); // Keep this at the top
import { bold, green, red } from 'ansis';
import { Command } from 'commander';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';
import { html2richtext } from './src/html2richtext';
import { richtextBlokTemplate } from './templates/richtext.template';

const FROM_RELEASE = 150420;
const STORYBLOK_RELEASE = 150420;

type HtmlBlock = {
  _uid: string;
  code: {
    code: string;
    language: string;
  };
  component: 'knowledge_html';
};

type Section = {
  _uid: string;
  content: Array<HtmlBlock | any>;
  component: string;
  vertical_rhythm: string;
};

const program = new Command();
program
  .name('convert-general-text-to-richtext')
  .description(
    'Loops through all sections in all articles and converts general content HTML markup to Storyblok richtext',
  )
  .option('-d, --dry-run', 'Run the script without actually coverting anything')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];
let allLinks: ISbStoryData[];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
      release_id: FROM_RELEASE,
      filter_query: {
        tag: {
          not_in: 'o2-compliance-scripts',
        },
      },
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {
        release_id: FROM_RELEASE,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function findAndUpdateHtmlBlok(story: ISbStoryData): Promise<any> {
  const storyBody = story.content.body;
  return processSections(storyBody);
}

const processSections = (sections: Section[]): Section[] => {
  return sections.map((section) => {
    if (!section.content || !Array.isArray(section.content)) {
      return section;
    }

    const newContent = section.content.flatMap((block) => {
      if (block?.code?.code.includes('<![CDATA')) {
        console.error('Found inline styles in HTML block');
        process.exit(1);
      }

      if (block.component !== 'knowledge_html') {
        return block;
      }

      // Unconvertable HTML blocks
      if (
        block.code.code.includes('<table') ||
        block.code.code.includes('embeddedObject')
      ) {
        return block;
      }

      const richtext = html2richtext(block.code.code);
      return richtextBlokTemplate(richtext);
    });

    return {
      ...section,
      content: newContent,
    };
  });
};

async function processArticles() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      !story.slug.includes('_common') &&
      !story.slug.includes('navigation')
    );
  });

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent?.content?.body;

    if (!storyBody) {
      console.log(`No body content found for ${story.name}`);
      continue;
    }

    if (!JSON.stringify(storyBody)?.includes('knowledge_html')) {
      console.log(`No knowledge_html components found in ${story.name}`);
      continue;
    }

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyContent);

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    console.log(green`Updating ${story.name}`);

    const updatedStory = {
      ...storyContent,
      content: {
        ...storyContent.content,
        body: updatedStoryBody,
      },
      publish: true,
      release_id: STORYBLOK_RELEASE,
    };

    if (!options.dryRun) {
      try {
        // Update the story with the new body
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          updatedStory,
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

processArticles();
