require('dotenv').config(); // Keep this at the top
import { blue, green, red, yellow } from 'ansis';
import { Command } from 'commander';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';
import { topicNavigation } from './templates/topic-navigation.template';

const program = new Command();
program
  .name('add-sub-topic-navs')
  .description(
    'Adds sub-topic navigation to the bottom of each topic navigation page in Storyblok',
  )
  .option('-d, --dry-run', 'Run the script without actually updating any links')
  .parse(process.argv);

const options = program.opts();

let allLinks: ISbLink[];
let topicNavPagesCreated = 0;

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];

/**
 * Get all links in a given folder slug
 */
async function getAllLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

/**
 * Get single story for given slug
 */
async function getStoryBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
      {
        starts_with: `${slug}`,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<ISbStory> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

/**
 * Find all folders and then loop through each folder, either creating a new nav page or updating an existing one, adding a section to the page containing a tile representing each sub folder. Each folder will have an article with slug /navigation within in and we should use the story uuid to link to.
 */
async function addNavPages() {
  allLinks = await getAllLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const folders = allLinks.filter((link) => link.is_folder);

  for (const folder of folders) {
    // Navigation page to update with sub folder tiles
    const navPage = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
      {
        by_slugs: `${folder.slug}/navigation`,
      },
    );
    const hasNavPage = navPage.data.stories.length > 0;

    // Get all sub folders where parent_id matches the current folder id
    const subFolders = allLinks.filter(
      (link) => link.parent_id === folder.id && link.is_folder,
    );

    // Continue if no sub folders are found
    if (subFolders.length === 0) {
      continue;
    }

    console.log(
      `Found ${subFolders.length.toString()} sub folders for ${folder.name}`,
    );

    const subNavPages = await Promise.all(
      subFolders.map((subFolder) =>
        getStoryBySlug(`${subFolder.slug}/navigation`).then((response) => {
          const story = response[0];

          if (!story) {
            console.error(red`No navigation page found for ${subFolder.name}`);
            console.log(`${subFolder.slug}/navigation`, response);
            return;
          }

          return {
            ...story,
            name: subFolder.name, // Override the name of the article to be the name of the containing subfolder
          };
        }),
      ),
    );

    const filteredSubNavPages = subNavPages.filter((navPage) => navPage);

    /**
     * !important - For anyone reading - this process has to be ran multiple times to build the tree structure from the inside out.
     * Note: I could have used recursion, but that makes my head hurt and I'm not getting paid enough for that
     */
    if (!hasNavPage) {
      if (filteredSubNavPages.length !== subFolders.length) {
        console.error(
          red`Skipping folder ${folder.name} as sub navs not ready yet`,
        );

        continue;
      }

      console.log(green`Creating navigation page for ${folder.name}...`);

      try {
        const newTopicNavPage = topicNavigation(
          'Navigation',
          [],
          filteredSubNavPages,
          folder.id.toString(),
          folder.name,
        );

        await storyblokClient.post(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
          newTopicNavPage,
        );

        console.log(green`Created navigation in folder: ${folder.name}`);

        topicNavPagesCreated++;
      } catch (error) {
        console.error(red`Error creating navigation page: ${error.message}`);
        console.log(filteredSubNavPages);
      }
    }

    if (hasNavPage) {
      console.log(yellow`Updating navigation page for ${folder.name}...`);

      const navPageID = navPage.data.stories[0].id;

      const navPageStory = await getStoryByID(navPageID);
      const navPageStoryData = navPageStory.data.story;
      const navPageStoryBody = navPageStoryData.content.body;

      const navPageSubTitle = navPageStoryBody[0].content[0].subtitle;

      if (navPageSubTitle !== 'Articles') {
        console.log(
          blue`Navigation page does not have 'Articles' as the first section, skipping nav page...`,
        );
        continue;
      }

      // Nav page is ready for additional sub folder tiles
      if (navPageStoryBody.length === 2) {
        console.log(green`Adding sub folder nav items to navigation page...`);

        const existingStories = navPageStoryBody[1].content[0].body.map(
          (tile) => ({
            name: tile.title,
            uuid: tile.link.id,
          }),
        );

        if (existingStories.length < 1) {
          console.error(
            red`No existing stories found in navigation page - something isn't right`,
          );
          break;
        }

        const updatedNavPage = topicNavigation(
          'Navigation',
          existingStories,
          filteredSubNavPages,
          folder.id.toString(),
          folder.name,
        );

        try {
          await storyblokClient.put(
            `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${navPageID}`,
            updatedNavPage,
          );

          console.log(green`Updated navigation in folder: ${folder.name}`);
        } catch (error) {
          console.error(red`Error updating navigation page: ${error.message}`);
        }
      }
    }
  }

  console.log(
    green`Created ${topicNavPagesCreated.toString()} topic navigation pages`,
  );
}

addNavPages();
