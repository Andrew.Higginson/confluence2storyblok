require('dotenv').config(); // Keep this at the top
import { bold, green, red } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';
import { fragmentBlok } from './templates/fragment.template';
import { htmlBlok } from './templates/html.template';
import { quicklinksBlokTemplate } from './templates/quicklinks.template';
import { sectionBlok } from './templates/section.template';
import { textBlokTemplate } from './templates/text.template';

const STORYBLOK_RELEASE = 146937;

const program = new Command();
program
  .name('convert-columns-to-sections')
  .description(
    'Loops through all columns in all articles and converts them to Storyblok sections',
  )
  .option('-d, --dry-run', 'Run the script without actually removing any links')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];
let allLinks: ISbStoryData[];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {},
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function getStoryBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
      {
        by_slugs: `${slug}`,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

const removeStellaInformationBox = (html: string): string => {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const informationBox = document.querySelector(
    '.confluence-information-macro-information',
  );

  if (informationBox) {
    informationBox.remove();
  }

  return document.body.innerHTML;
};

function extractRelatedArticleLinks(
  html: string,
): { title: string; url: string }[] {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const links = Array.from(
    document.querySelectorAll('.content-by-label a') as HTMLAnchorElement[],
  );

  const extractedLinks = links.map((link) => {
    return {
      title: link.textContent || '',
      url: link.href,
    };
  });

  return extractedLinks;
}

/**
 * Takes a string of HTML and converts all elements that match the following: .content .contentLayout2 .columnLayout .innerCell
 * and convert them to an array of HTML elements and forgetting anything else
 */
function convertColumnsToSections(html: string): string[] {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const columns = Array.from(
    document.querySelectorAll(
      '.contentLayout2 .columnLayout .innerCell',
    ) as HTMLLIElement[],
  );

  console.log(`Found ${columns.length} columns`);

  const sections = columns.map((column) => {
    if (!column) {
      return '';
    }

    const columnContent = column.innerHTML;
    if (!columnContent) {
      return '';
    }

    return columnContent.trim();
  });

  return sections;
}

/**
 * Recursively loop through a nested object finding all objects where the 'component' property equals 'knowledge_html' and then flatten them into an array of HTML strings. If a property is an array, try to process it recurssively.
 */
type Code = {
  code: string;
  title: string;
  language: string;
};

type Content = {
  _uid: string;
  code?: Code;
  component: string;
};

type BodyItem = {
  _uid: string;
  content: Content[];
  component: string;
  vertical_rhythm?: string;
};

function extractHTMLSections(body: BodyItem[]): string[] {
  const htmlBlocks: string[] = [];

  body.forEach((item) => {
    if (item.content && Array.isArray(item.content)) {
      item.content.forEach((contentItem) => {
        if (contentItem.component === 'knowledge_html' && contentItem.code) {
          htmlBlocks.push(contentItem.code.code);
        }
      });
    }
  });

  return htmlBlocks;
}

/**
 * Using JSDoc, remove all divs where the ID contains 'RelatedArticles'
 */
const removeRelatedArticlesSection = (html: string): string => {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const relatedArticles = document.querySelector('div[id*="RelatedArticles"]');

  if (relatedArticles) {
    relatedArticles.remove();
  }

  return document.body.innerHTML;
};

async function processArticles() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks
    .filter((story: ISbLink) => {
      return (
        story.is_folder === false &&
        !story.slug.includes('_common') &&
        !story.slug.includes('navigation')
      );
    })
    .slice(795, 871);

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    if (!storyBody || !JSON.stringify(storyBody).includes('innerCell')) {
      options.debug && console.log(`No HTML to uplift ${story.name}`);
      continue;
    }

    const htmlSections = extractHTMLSections(storyBody).map((section) => {
      if (section.includes('confluence-information-macro-information')) {
        return removeStellaInformationBox(section);
      }

      return section;
    });

    const links = extractRelatedArticleLinks(htmlSections.join('\n'))
      .map((link) => allLinks.find((story) => story.slug.includes(link.url)))
      .filter((link) => link?.slug)
      // Filter out any duplicates
      .filter(
        (link, index, self) =>
          index === self.findIndex((t) => t.slug === link.slug),
      );

    /**
     * Extract all HTML sections from the body, convert them to sections and then convert them to HTML bloks
     */
    const newSections = htmlSections.flatMap((html) =>
      convertColumnsToSections(html)
        .filter((section) => {
          // Most specific thing I can target related articles with
          return !section.includes('com.adaptavist.confluence.rate');
        })
        .map((column) => sectionBlok([htmlBlok(column)])),
    );

    console.log(green`Updating ${story.name}`);

    const updatedStory = {
      ...storyContent,
      content: {
        ...storyContent.content,
        body: [
          ...newSections,
          sectionBlok([fragmentBlok('547fea2c-461b-4087-bebe-d6363d4855ae')]),
          sectionBlok([
            textBlokTemplate('Related Articles'),
            quicklinksBlokTemplate(links),
          ]),
        ],
      },
      publish: true,
      release_id: STORYBLOK_RELEASE,
    };

    // console.log(JSON.stringify(updatedStory, null, 2));

    if (!options.dryRun) {
      try {
        // Update the story with the new body
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          updatedStory,
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

processArticles();
