require('dotenv').config(); // Keep this at the top
import { blueBright, greenBright, red, redBright } from 'ansis';
import { Command } from 'commander';
import { createObjectCsvWriter } from 'csv-writer';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';

const STORYBLOK_RELEASE = 153097;

const program = new Command();
program
  .name('convert-general-text-to-richtext')
  .description(
    'Loops through all sections in all articles and converts general content HTML markup to Storyblok richtext',
  )
  .option(
    '-d, --dry-run',
    'Run the script without actually converting anything',
  )
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

const csvWriter = createObjectCsvWriter({
  path: 'updated_links.csv',
  header: [
    { id: 'name', title: 'Story Name' },
    { id: 'uuid', title: 'Story UUID' },
    { id: 'linkText', title: 'Link Text' },
    { id: 'originalUrl', title: 'Original URL' },
    { id: 'decision', title: 'Decision' },
  ],
  append: false, // Ensure the headers are included
});

let allLinks: ISbStoryData[];

async function writeCsvLine(
  name: string,
  uuid: string,
  linkText: string,
  originalUrl: string,
  decision: string,
) {
  await csvWriter.writeRecords([
    { name, uuid, linkText, originalUrl, decision },
  ]);
}

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
    });
    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {},
    );
    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

const findAndUpdateRichtextLinks = async (
  obj: any,
  storyName: string,
  storyUUID: string,
  parentText: string,
): Promise<any> => {
  if (typeof obj !== 'object' || obj === null) {
    return obj;
  }

  if (Array.isArray(obj)) {
    return Promise.all(
      obj.map((item) =>
        findAndUpdateRichtextLinks(item, storyName, storyUUID, parentText),
      ),
    );
  }

  if (obj.type === 'link') {
    return processLink(obj, storyName, storyUUID, parentText);
  }

  if (obj.type === 'text') {
    parentText = obj.text;
  }

  const updatedObj = {};
  for (const [key, value] of Object.entries(obj)) {
    updatedObj[key] = await findAndUpdateRichtextLinks(
      value,
      storyName,
      storyUUID,
      parentText,
    );
  }
  return updatedObj;
};

const getUrlPath = (url: string): string => {
  try {
    const parsedUrl = new URL(url);
    return parsedUrl.pathname + parsedUrl.hash;
  } catch (e) {
    return url;
  }
};

async function processLink(
  link,
  storyName: string,
  storyUUID: string,
  linkText: string,
) {
  if (!link.attrs.href) {
    console.log(redBright`No href found for link`);
    await writeCsvLine(
      storyName,
      storyUUID,
      linkText,
      'No href found',
      'untouched',
    );
    return link;
  }

  if (
    link.attrs.href.startsWith('http') &&
    !link.attrs.href.includes('vmo2digital.co.uk')
  ) {
    console.log(redBright`External link found: ${link.attrs.href}`);
    await writeCsvLine(
      storyName,
      storyUUID,
      linkText,
      link.attrs.href,
      'external url',
    );
    return link;
  }

  const url = getUrlPath(link.attrs.href);
  const href = link.attrs.href;
  const parts = url.split('#');
  const urlWithoutAnchor = parts[0];
  const anchor = parts[1];

  const foundLink = allLinks.find((story) =>
    story.slug.endsWith(urlWithoutAnchor),
  );

  if (foundLink) {
    console.log(
      greenBright`Link updated to ${foundLink.name} (${foundLink.slug})`,
    );

    await writeCsvLine(storyName, storyUUID, linkText, href, 'converted');

    const newLink = {
      type: 'link',
      ...link,
      attrs: {
        ...link.attrs,
        href: urlWithoutAnchor,
        uuid: foundLink.uuid,
        anchor,
        target: '_self',
        title: foundLink.name,
        linktype: 'story',
      },
    };

    return newLink;
  }

  console.log(redBright`Link not found for ${urlWithoutAnchor}`);

  await writeCsvLine(storyName, storyUUID, linkText, href, 'untouched');

  return link;
}

async function processArticles() {
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      !story.slug.includes('_common') &&
      !story.slug.includes('navigation')
    );
  });

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      blueBright`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent?.content?.body;

    if (!storyBody) {
      console.log(`No body content found for ${story.name}`);
      continue;
    }

    const updatedStoryBody = await findAndUpdateRichtextLinks(
      storyBody,
      story.name,
      story.uuid,
      '',
    );

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    console.log(blueBright`Updating ${story.name}`);

    const updatedStory = {
      ...storyContent,
      content: {
        ...storyContent.content,
        body: updatedStoryBody,
      },
      publish: true,
      release_id: STORYBLOK_RELEASE,
    };

    if (!options.dryRun) {
      try {
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          updatedStory,
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

processArticles();
