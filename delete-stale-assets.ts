require('dotenv').config(); // Keep this at the top
import { grey, red, yellow } from 'ansis';
import { Command } from 'commander';
import StoryblokClient from 'storyblok-js-client';

const program = new Command();
program
  .name('delete-stale-assets')
  .description(
    'Delete assets that were imported from Confluence but are no longer used',
  )
  .option(
    '-d, --dry-run',
    'Run the script without actually deleting the assets',
  )
  .action((options) => {
    deleteStaleAssets(options.dryRun);
  })
  .parse(process.argv);

const options = program.opts();

async function deleteStaleAssets(dryRun = false) {
  const storyblokClient = new StoryblokClient({
    accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
    oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
  });

  const response = await storyblokClient.get(
    `spaces/${process.env.STORYBLOK_SPACE_ID}/assets/`,
    {
      search: 'o2',
      per_page: 1000,
      sort_by: 'created_at:desc',
    },
  );

  const assets = response.data?.assets as any[];

  const filteredAssets = assets
    .filter((asset) => asset.filename.match(/o2.*\d{8}\.\w{3,4}$/))
    .filter((asset) => asset.meta_data?.imported_from === 'O2 Confluence')
    // .filter((asset) => !asset.meta_data?.imported_from)
    // .filter(
    //   (asset) =>
    //     asset.asset_folder_id !== parseInt(process.env.PARENT_ASSET_FOLDER_ID),
    // )
    .filter(
      (asset) =>
        new Date(asset.created_at).getTime() <
        new Date().getTime() - 7 * 24 * 60 * 60 * 1000,
    );

  console.log(`Deleting ${filteredAssets.length} stale assets...`);

  // Pause until user confirms they'd like to continue to delete the assets

  const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  if (filteredAssets.length === 0) {
    console.log('No stale assets found to delete.');
    process.exit(0);
  }

  if (options.dryRun) {
    console.log(yellow`RUNNING IN DRY RUN MODE - NO ASSETS WILL BE DELETED`);
  }

  readline.question(
    `Are you sure you want to delete ${filteredAssets.length} stale assets? (y/n) `,
    async (answer) => {
      if (answer.toLowerCase() !== 'y') {
        console.log('Aborting...');
        process.exit(1);
      }

      for (const [index, asset] of filteredAssets.entries()) {
        if (options.dryRun) {
          console.log(
            grey`Dry run: would delete asset ${(index + 1).toString()} of ${filteredAssets.length.toString()} ${asset.filename}...`,
          );
          continue;
        }

        console.log(
          red`Deleting asset ${(index + 1).toString()} of ${filteredAssets.length.toString()} ${asset.filename}...`,
        );

        try {
          // Delete asset from Storyblok
          await storyblokClient.delete(
            `spaces/${process.env.STORYBLOK_SPACE_ID}/assets/${asset.id}`,
            {},
          );
        } catch (error: unknown) {
          console.error(
            red`Error deleting asset ${asset.filename}: ${JSON.stringify(error)}`,
          );
        }
      }

      readline.close();
    },
  );
}
