declare global {
  namespace NodeJS {
    interface ProcessEnv {
      STORYBLOK_OAUTH_TOKEN: string;
      STORYBLOK_ACCESS_TOKEN: string;
      STORYBLOK_SPACE_ID: string;
      PARENT_STORY_FOLDER_ID: string;
      PARENT_ASSET_FOLDER_ID: string;
      PARENT_TOPIC_FOLDER_ID: string;
      TARGET_BASE_URL_PATH: string;
      STORYBLOK_BASE_SLUG: string;
    }
  }
}

export {};
