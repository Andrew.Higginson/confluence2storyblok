require('dotenv').config(); // Keep this at the top
import { blue, bold, green, redBright } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import slugify from 'slugify';
import StoryblokClient, { ISbStory, ISbStoryData } from 'storyblok-js-client';

const STORYBLOK_RELEASE = 145427;

let allLinks: ISbStoryData[] = [];

const program = new Command();
program
  .name('fix-confluence-create-action-links')
  .description(
    'Fix Confluence links in Storyblok by updating the asset URLs to the new location',
  )
  .option('-d, --dry-run', 'Run the script without actually updating any links')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];

// Articles that are linked but do not exist in storyblok
const knownMissingSlugs = ['new-search-tool-announcement'];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
      release_id: STORYBLOK_RELEASE,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {
        release_id: STORYBLOK_RELEASE,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function getStoryBySlug(slug: string): Promise<any> {
  console.log(`Getting story by slug: ${slug}`);
  try {
    const response = await storyblokClient.get(`cdn/stories`, {
      by_slugs: slug,
      release_id: STORYBLOK_RELEASE,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function updateLinksInHtml(html: string): Promise<string> {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const links = Array.from(
    document.querySelectorAll('a[href]') as HTMLAnchorElement[],
  );

  for (const link of links) {
    const href = link.getAttribute('href');
    const match = href.match(/\/display\/.+\/([^#]*)(#.*)?/);

    if (match) {
      const url = match[1];

      const fragment = match[2] ? match[2].replace('#', '#ArtPage') : '';

      const slug = slugify(
        decodeURIComponent(url).replace(`'`, '').replace(/\+/g, '-'),
        { lower: true, strict: true },
      );

      if (knownMissingSlugs.includes(slug)) {
        console.log(redBright`Skipping known missing slug: ${slug}`);
        continue;
      }

      console.log(blue`Matched URL (${url}) and will update to: ${slug}`);

      const story = allLinks.find((story) => story.slug.includes(slug));

      const fullSlug = story ? story.slug : href;
      const urlWithoutDomain = `/${fullSlug.split('/').slice(1).join('/')}${fragment}`;

      if (story) {
        console.log(
          green`Updating ${slug} (${href}) with slug: ${urlWithoutDomain}`,
        );
      } else {
        if (options.debug) {
          console.log(redBright`No match for ${slug} (${href})`);
        }
      }

      link.setAttribute('href', urlWithoutDomain);
    }
  }

  return dom.serialize();
}

async function findAndUpdateHtmlBlok(obj: any): Promise<any> {
  function deepClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  async function updateNestedObject(nestedObj: any): Promise<void> {
    for (const key in nestedObj) {
      if (typeof nestedObj[key] === 'object' && nestedObj[key] !== null) {
        updateNestedObject(nestedObj[key]);
      } else if (nestedObj.component === 'knowledge_html') {
        const html = nestedObj.code.code;
        if (html) {
          nestedObj.code.code = await updateLinksInHtml(html);
        }
      }
    }
  }

  const clonedObj = deepClone(obj);
  await updateNestedObject(clonedObj);
  return clonedObj;
}

async function updateLinks() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter(
    (story: ISbStoryData) =>
      story.is_folder === false && !story.slug.includes('common'),
  );

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyBody);

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    if (!options.dryRun) {
      // Update the story with the new body
      await storyblokClient.put(
        `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
        {
          ...storyContent,
          content: {
            ...storyContent.content,
            body: updatedStoryBody,
          },
          release_id: STORYBLOK_RELEASE,
          publish: true,
        },
      );
    }
  }
}

updateLinks();
