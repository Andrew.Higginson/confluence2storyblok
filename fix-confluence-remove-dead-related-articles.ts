require('dotenv').config(); // Keep this at the top
import { bold, green, red } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';

const STORYBLOK_RELEASE = 145687;

const program = new Command();
program
  .name('remove-dead-related-articles')
  .description(
    'Loops through all related article linka and removes those that no longer exist in Storyblok',
  )
  .option('-d, --dry-run', 'Run the script without actually removing any links')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];
let allLinks: ISbLink[];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {},
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function getStoryBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
      {
        by_slugs: `${slug}`,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function pruneRelatedArticles(
  html: string,
  full_slug: string,
): Promise<string> {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const relatedArticles = Array.from(
    document.querySelectorAll('ul.content-by-label li') as HTMLLIElement[],
  );

  for (const article of relatedArticles) {
    if (!article) {
      continue;
    }

    const anchor = article.querySelector('a') as HTMLAnchorElement;
    if (!anchor) {
      continue;
    }

    const href = anchor.getAttribute('href');

    const story = allLinks.find((s) => s.slug.endsWith(href));

    if (story) {
      console.log(green`Found story for related article ${href}`);
      continue;
    }

    if (!story) {
      console.log(red`No story found for related article ${href}, removing...`);
      article.remove();
    }
  }

  return dom.serialize();
}

async function findAndUpdateHtmlBlok(story: ISbStoryData): Promise<any> {
  const storyBody = story.content.body;

  function deepClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  async function updateNestedObject(nestedObj: any): Promise<void> {
    for (const key in nestedObj) {
      if (typeof nestedObj[key] === 'object' && nestedObj[key] !== null) {
        updateNestedObject(nestedObj[key]);
      } else if (nestedObj.component === 'knowledge_html') {
        const html = nestedObj.code.code;
        if (html) {
          nestedObj.code.code = await pruneRelatedArticles(
            html,
            story.full_slug,
          );
        }
      }
    }
  }

  const clonedObj = deepClone(storyBody);
  await updateNestedObject(clonedObj);
  return clonedObj;
}

async function removeLinks() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      !story.slug.includes('_common') &&
      !story.slug.includes('navigation')
    );
  });

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    if (!JSON.stringify(storyBody).includes('content-by-label')) {
      options.debug && console.log(`No links to update for ${story.name}`);
      continue;
    }

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyContent);

    if (JSON.stringify(updatedStoryBody) === JSON.stringify(storyBody)) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    const updatedStory = {
      ...storyContent,
      content: {
        ...storyContent.content,
        body: updatedStoryBody,
      },
    };

    console.log(green`Updating ${story.name}`);

    if (!options.dryRun) {
      try {
        // Update the story with the new body
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          {
            ...updatedStory,
            publish: true,
            release_id: STORYBLOK_RELEASE,
          },
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

removeLinks();
