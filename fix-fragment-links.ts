require('dotenv').config(); // Keep this at the top
import { blue, bold } from 'ansis';
import { Command } from 'commander';
import console from 'console';
import { JSDOM } from 'jsdom';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';

const STORYBLOK_RELEASE = 144422;

const program = new Command();
program
  .name('fix-confluence-links')
  .description(
    'Fix fragment links in Storyblok to ensure current page is part of the link URL',
  )
  .option('-d, --dry-run', 'Run the script without actually updating any links')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {},
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

async function updateLinksInHtml(
  html: string,
  full_slug: string,
): Promise<string> {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const links = Array.from(
    document.querySelectorAll('a[href^="#ArtPage"]') as HTMLAnchorElement[],
  );
  const articlePath = full_slug.replace(
    'vmo2digital-co-uk/o2-knowledge-hub/',
    '',
  );

  for (const link of links) {
    const href = link.getAttribute('href');
    const newHref = `${articlePath}${href}`;

    console.log(`Updating link: ${href} => ${newHref}`);

    link.setAttribute('href', newHref);
  }

  return dom.serialize();
}

async function findAndUpdateHtmlBlok(story: ISbStoryData): Promise<any> {
  const storyBody = story.content.body;

  function deepClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  async function updateNestedObject(nestedObj: any): Promise<void> {
    for (const key in nestedObj) {
      if (typeof nestedObj[key] === 'object' && nestedObj[key] !== null) {
        updateNestedObject(nestedObj[key]);
      } else if (nestedObj.component === 'knowledge_html') {
        const html = nestedObj.code.code;
        if (html) {
          nestedObj.code.code = await updateLinksInHtml(html, story.full_slug);
        }
      }
    }
  }

  const clonedObj = deepClone(storyBody);
  await updateNestedObject(clonedObj);
  return clonedObj;
}

async function updateLinks() {
  // First fetch all story references using the link API
  const stories: ISbLink[] = await getAllStoryLinksBySlug(
    process.env.STORYBLOK_BASE_SLUG,
  );

  const filteredStories = stories.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      !story.slug.includes('_common') &&
      !story.slug.includes('navigation')
    );
  });

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${stories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    if (!JSON.stringify(storyBody).includes('href=\\"#ArtPage')) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    console.log(blue`Updating links for: ${story.name}`);

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyContent);

    if (updatedStoryBody === storyBody) {
      options.debug && console.log(`No changes detected for ${story.name}`);
      continue;
    }

    if (options.dryRun) {
      console.log(blue`Would update ${story.name}`);
      continue;
    }

    // Update the story with the new body
    await storyblokClient.put(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
      {
        ...storyContent,
        content: {
          ...storyContent.content,
          body: updatedStoryBody,
        },
        release_id: STORYBLOK_RELEASE,
        publish: true,
      },
    );
  }
}

updateLinks();
