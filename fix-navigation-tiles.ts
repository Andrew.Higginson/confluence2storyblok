require('dotenv').config(); // Keep this at the top
import { bold, red } from 'ansis';
import { Command } from 'commander';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';

const STORYBLOK_RELEASE = 144930;

const program = new Command();
program
  .name('fix-navigation-icons')
  .description('Fixes all tiles on the navigation pages to the correct icon')
  .option('-d, --dry-run', 'Run the script without actually updating any tiles')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];
let allLinks: ISbLink[];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {},
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

interface Component {
  component?: string;
  name?: string;
  content?: Component[];
  body?: Component[];
  [key: string]: any;
}

function updateDynamoTiles(obj: Component): Component {
  if (obj.component === 'dynamo_tile' && obj.icon.name === 'Memo circle info') {
    obj.icon = {
      name: 'Memo circle info',
      value:
        'https://a.storyblok.com/f/253875/169x150/caabf901e9/memo-circle-info.svg',
      iconSet: 'Light',
      dimension_value:
        'https://a.storyblok.com/f/253875/169x150/caabf901e9/memo-circle-info.svg',
    };
  }

  if (obj.component === 'dynamo_tile' && obj.icon.name === 'Grid 2 plus') {
    obj.icon = {
      name: 'Grid 2 plus',
      value:
        'https://a.storyblok.com/f/253875/150x150/d6a4ccdf8c/grid-2-plus.svg',
      iconSet: 'Light',
      dimension_value:
        'https://a.storyblok.com/f/253875/150x150/d6a4ccdf8c/grid-2-plus.svg',
    };
  }

  // Recursively process child components in 'content' and 'body' arrays
  if (Array.isArray(obj.content)) {
    obj.content = obj.content.map(updateDynamoTiles);
  }

  if (Array.isArray(obj.body)) {
    obj.body = obj.body.map(updateDynamoTiles);
  }

  return obj;
}

async function updateLinks() {
  // First fetch all story references using the link API
  allLinks = await getAllStoryLinksBySlug(process.env.STORYBLOK_BASE_SLUG);

  const filteredStories = allLinks.filter((story: ISbLink) => {
    return (
      story.is_folder === false &&
      story.slug.includes('navigation') &&
      !story.slug.includes('common')
    );
  });

  console.log(`Found ${filteredStories.length} navigation pages`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${filteredStories.length.toString()}] ${story.slug}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyData = fullStory.data.story as ISbStoryData;
    const storyContent = storyData.content;

    const updatedStoryContent = updateDynamoTiles(storyContent);

    const updatedStory = {
      ...storyContent,
      content: storyContent,
    };

    if (!options.dryRun) {
      try {
        // Update the story with the new body
        await storyblokClient.put(
          `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
          {
            ...updatedStory,
            release_id: STORYBLOK_RELEASE,
            publish: true,
          },
        );
      } catch (error) {
        console.error(red`Error updating story: ${error.message}`);
        continue;
      }
    }
  }
}

updateLinks();
