require('dotenv').config(); // Keep this at the top

import { blue, bold, green, red, yellow } from 'ansis';
import { existsSync, readFileSync } from 'node:fs';
import { ISbStoryData } from 'storyblok-js-client';
import { program } from './src/init';
import { ArticleData } from './src/interfaces';
import { stats } from './src/stats';
import {
  createStoryInStoryblok,
  createTopicNav,
  getStoryBySlug,
  getStoryblokFolderId,
  swapAssetsInHtml,
} from './src/storyblok';
import {
  extractArticleContent,
  getHtmlFiles,
  removeArticleTypePrefix,
  removeExportElement,
  removeInlineCommentSpans,
  removePageTypePrefix,
  removeRaterElement,
  replaceEmoticonsWithEmojis,
  updateLocalHyperlinks,
} from './src/utils';

const publishedStories: {
  [key: string]: {
    folderId: string;
    folderName: string;
    stories: ISbStoryData[];
  };
} = {};
const errorMessages = [] as string[];

program
  .action((inputFile, options) => {
    console.log(bold(`Importing stories from: ${inputFile}...`));
    importStories(inputFile, options.debug, options.dryRun);
  })
  .parse();

/**
 * Processors to clean the HTML before importing
 */
const processors = [
  swapAssetsInHtml, // Find all assets and upload them to Storyblok
  removeRaterElement, // Remove the Confluence 'rater' element from bottom of page
  updateLocalHyperlinks, // Convert local HTML links to article slug paths for later processing
  removeExportElement, // Remove the 'Storyblok Export' element from the related articles
  removePageTypePrefix, // Remove the 'ArtPage-' or 'NavPage-' prefix from the file name
  // updateHyperlinkPaths, // Update the paths of hyperlinks to point to the new Storyblok URLs
  removeInlineCommentSpans, // Remove confluence 'inline comments' from the HTML markup
  replaceEmoticonsWithEmojis, // Replace emoticon images with emojis
];

/**
 * Clean the HTML before importing
 */
async function cleanHtml(
  html: string,
  dirPath: string,
  articleSlug: string,
): Promise<string> {
  let cleanedHtml = html;

  for (let processor of processors) {
    try {
      cleanedHtml = await processor(cleanedHtml, dirPath, articleSlug);
    } catch (error) {
      const errorMessage = `Error processing HTML with processor ${processor.name}: ${error}`;
      console.error(red(errorMessage));
      errorMessages.push(errorMessage);
    }
  }

  return cleanedHtml;
}

/**
 * Handle the import of stories to storyblok
 */
async function importStories(
  inputDir: string,
  debug: boolean,
  dryRun: boolean,
) {
  try {
    if (!existsSync(inputDir)) {
      throw new Error(`Directory not found: ${inputDir}`);
    }

    const files = (await getHtmlFiles(inputDir))
      .filter(
        (file) => !file.endsWith('index.html'), // Exclude the index.html file
      )
      .filter(
        (file) => !file.includes('/attachments/'), // Exclude the attachments directory
      );

    for (const [index, file] of files.entries()) {
      console.log(
        bold`Processing file ${(index + 1).toString()} of ${files.length.toString()}: ${file}...`,
      );

      const fileContent = await readFileSync(file, 'utf-8');

      let article: ArticleData;

      const absoluteDirPath = file.split('/').slice(0, -1).join('/');
      const relativeDirPath = file
        .split(inputDir)
        .pop()
        .split('/')
        .slice(0, -1)
        .filter(Boolean)
        .join('/');

      const fileName = file.split('/').pop();

      try {
        article = await extractArticleContent(
          fileContent,
          `${relativeDirPath}/${fileName}`,
        );

        // console.log(JSON.stringify(article, null, 2));
      } catch (error) {
        console.error(red(`Error processing file: ${file} = `), error);
        continue;
      }

      const articlePathParts = article.navigation // Path derived from the article navigation breadcrumbs
        .map((dir) => removeArticleTypePrefix(dir)); // Remove the 'ArtPage-' or 'NavPage-' prefix
      const flattenedPath = articlePathParts.join('/'); // Flatten the path into a single string (careful for articles that contain '/')

      /**
       * Create the story in Storyblok
       */
      if (!dryRun) {
        const existingStory = await getStoryBySlug(
          `${process.env.STORYBLOK_BASE_SLUG}/${article.fullSlug}`,
        );

        if (existingStory) {
          console.log(
            blue`Story already exists: ${article.title} (${article.slug})`,
          );

          publishedStories[flattenedPath] = {
            folderId: existingStory.parent_id,
            folderName: articlePathParts[articlePathParts.length - 1],
            stories: [
              ...(publishedStories[flattenedPath]?.stories ?? []),
              existingStory,
            ],
          };

          continue;
        }

        debug &&
          console.log(
            `Cleaning HTML for: ${article.title} in folder ${article.folderName}...`,
          );

        const cleanedHtml = await cleanHtml(
          article.html,
          absoluteDirPath,
          article.slug,
        );

        debug &&
          console.log(
            yellow`Creating parent folder structure for ${flattenedPath}...`,
          );

        const parentFolderId = await getStoryblokFolderId(articlePathParts);

        try {
          const story = await createStoryInStoryblok(
            {
              ...article,
              html: cleanedHtml,
            },
            parentFolderId,
          );

          stats.createdStoryCount++;

          // Build structure of published stories for topic navigation
          publishedStories[flattenedPath] = {
            folderId: parentFolderId,
            folderName: articlePathParts[articlePathParts.length - 1],
            stories: [
              ...(publishedStories[flattenedPath]?.stories ?? []),
              story,
            ],
          };

          console.log(green`Story created: ${article.title}`);
        } catch (error) {
          const errorMessage = `Error creating story: ${JSON.stringify(error.message)}`;
          console.error(red(errorMessage));
          errorMessages.push(errorMessage);
        }
      }
    }

    /**
     * Finally, create the topic navigation sturcture in Storyblok
     */
    if (!dryRun) {
      // console.log(JSON.stringify(publishedStories, null, 2));

      // Loop over publishedStories and create topic navigation using createTopicNav for each article
      await Promise.all(
        Object.entries(publishedStories).map(
          async ([articlePath, { folderId, folderName, stories }]) => {
            console.log(
              green`Creating topic navigation for: ${articlePath} (${stories.length.toString()} stories)...`,
            );

            const articlePages = stories.filter(Boolean);

            try {
              await createTopicNav(folderName, articlePages, folderId);
            } catch (error) {
              const errorMessage = `Error creating topic navigation for ${articlePath}: ${error}`;
              console.error(red(errorMessage));
              errorMessages.push(errorMessage);
            }
          },
        ),
      );
    }

    if (errorMessages.length > 0) {
      console.error(red('Errors occurred during import:'));
      errorMessages.forEach((message) => console.error(red(message)));
    } else {
      console.log(green('Stories imported successfully!'));
    }
  } catch (error) {
    console.error(red('Unhandled error whilst importing stories:'), error);
  } finally {
    console.log(
      bold(
        `Import finished in ${((Date.now() - stats.startTime) / 1000).toFixed(2)}s`,
      ),
    );

    console.table({
      'Stories created': stats.createdStoryCount,
      'Folders created': stats.createdFolderCount,
      'Assets created': stats.createdAssetCount,
      'Navigation pages created': stats.createdNavigationPagesCount,
    });
  }
}
