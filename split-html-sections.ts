require('dotenv').config(); // Keep this at the top
import { bold } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import StoryblokClient, { ISbStory, ISbStoryData } from 'storyblok-js-client';

const program = new Command();
program
  .name('split-html-sections')
  .description(
    'Bulk update HTML based stories in Storyblok to break out HTML into Section Bloks',
  )
  .option(
    '-d, --dry-run',
    'Run the script without actually updating the stories',
  )
  .parse(process.argv);

const options = program.opts();

let filteredStories: ISbStoryData[] = [];

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
      cv: Date.now(), // Do not use cache
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

/**
 * Takes a HTML body and return an array of html segments, split by the class 'columnLayout single'
 */
async function splitHtmlSections(html: string): Promise<string[]> {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const body = document.body;

  const sections: HTMLDivElement[] = Array.from(
    body.querySelectorAll('.columnLayout.single'),
  ).map((section) => section.outerHTML);

  return sections;
}

async function updateStories() {
  // First fetch all story references using the link API
  const stories: ISbStoryData[] = await getAllStoryLinksBySlug(
    process.env.STORYBLOK_BASE_SLUG,
  );

  console.log(`Found ${stories.length} stories`);

  filteredStories = stories.filter(
    (story: ISbStoryData) => story.is_folder === false,
  );

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${stories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    // Loop through the HTML and split all divs where the class is 'columnLayout single' into separate section bloks
    const updatedStoryBody = await splitHtmlSections(storyBody);

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    if (!options.dryRun) {
      // Update the story with the new body
      await storyblokClient.put(
        `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
        {
          ...storyContent,
          content: {
            ...storyContent.content,
            body: updatedStoryBody,
          },
        },
      );
    }
  }
}

updateStories();
