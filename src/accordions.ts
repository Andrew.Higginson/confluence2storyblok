import { JSDOM } from 'jsdom';

export interface ExpandContainer {
  title: string;
  body: string;
  before: string;
  after: string;
}

export function html2accordion(html: string): ExpandContainer[] {
  const dom = new JSDOM(html);
  const document = dom.window.document;

  const expandContainers: ExpandContainer[] = [];

  const expandContainerElements =
    document.querySelectorAll('.expand-container');

  let previousSiblingHTML = '';
  let currentHTML = html;

  expandContainerElements.forEach((expandContainerElement, index) => {
    const expandControlTextElement = expandContainerElement.querySelector(
      '.expand-control-text',
    );
    const expandContentElement =
      expandContainerElement.querySelector('.expand-content');

    const title = expandControlTextElement
      ? expandControlTextElement.textContent?.trim() || ''
      : '';
    const body = expandContentElement ? expandContentElement.innerHTML : '';

    // Capture the HTML before the current expand-container
    const containerStartIndex = currentHTML.indexOf(
      expandContainerElement.outerHTML,
    );
    const beforeHTML =
      previousSiblingHTML + currentHTML.slice(0, containerStartIndex);
    currentHTML = currentHTML.slice(
      containerStartIndex + expandContainerElement.outerHTML.length,
    );

    // Set the before property for the current object
    const before = index === 0 ? beforeHTML : '';

    // Calculate the after property for the last expand-container
    const after =
      index === expandContainerElements.length - 1 ? currentHTML : '';

    expandContainers.push({ title, body, before, after });

    // Save remaining HTML for the next iteration
    previousSiblingHTML = '';
  });

  return expandContainers;
}
