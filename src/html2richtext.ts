import { markdownToRichtext } from 'storyblok-markdown-richtext';
import TurndownService from 'turndown';

const turndownService = new TurndownService();

export const html2richtext = (html: string) => {
  const markdown = turndownService.turndown(html);
  return markdownToRichtext(markdown);
};
