import { program } from 'commander';
import pkgInfo from '../package.json';

// Check for required environment variables
for (const envVar of [
  'PARENT_ASSET_FOLDER_ID',
  'PARENT_TOPIC_FOLDER_ID',
  'PARENT_STORY_FOLDER_ID',
  'STORYBLOK_OAUTH_TOKEN',
  'STORYBLOK_ACCESS_TOKEN',
  'STORYBLOK_SPACE_ID',
  'TARGET_BASE_URL_PATH',
  'STORYBLOK_BASE_SLUG',
]) {
  if (!process.env[envVar]) {
    program.error(`Missing environment variable: ${envVar}`);
  }
}

/**
 * Parse command line arguments
 */
program
  .name(pkgInfo.name)
  .description(pkgInfo.description)
  .version(pkgInfo.version)
  .argument('<input-dir>', 'Directory to import HTML data from')
  .option('--debug', 'Show extra log output')
  .option('--dry-run', 'Process the imports but do not create any stories')
  .option('--force', 'Force the import to continue even if there are errors')
  .parse();

const options = program.opts();

export { options, program };
