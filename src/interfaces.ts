export type ArticleType = 'article' | 'navigation';

export interface ImageSize {
  width: number;
  height: number;
}

export interface ArticleData {
  title: string;
  slug: string;
  fullSlug: string;
  html?: string;
  navigation?: string[];
  filePath: string;
  dirPath: string;
  folderName: string;
  articleType: ArticleType;
}
