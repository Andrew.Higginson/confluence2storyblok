/**
 * Stats for the current run
 */
export const stats = {
  startTime: Date.now(),
  createdStoryCount: 0,
  createdFolderCount: 0,
  createdAssetCount: 0,
  createdNavigationPagesCount: 0,
};
