import { blue, green, yellow } from 'ansis';
import FormData from 'form-data';
import gm from 'gm';
import { existsSync, readFileSync } from 'node:fs';
import StoryblokClient, { ISbStoryData } from 'storyblok-js-client';
import { dynamicPage } from '../templates/dynamic-page.template';
import { topicNavigation } from '../templates/topic-navigation.template';
import { options } from './init';
import { ArticleData } from './interfaces';
import { stats } from './stats';
import { createSlug, getFirstFileByExtension, getSize } from './utils';

interface DirectoryCache {
  [key: string]: string; // The key is the directory path, and the value is the Storyblok folder ID
}
const cache: DirectoryCache = {};

// Create a new Storyblok client
export const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

/**
 * Get the folder ID for a given path, creating the entire path as we go
 * if it doesn't already exist
 */
export const getStoryblokFolderId = async (
  pathParts: string[],
): Promise<string> => {
  let currentPath = '';
  let parentId = process.env.PARENT_STORY_FOLDER_ID;
  const flattenedPath = pathParts.join('/');

  console.log(`Getting folder ID for path: ${flattenedPath}...`);

  // Return the folder ID from the cache if it exists
  if (cache[flattenedPath]) {
    console.log(
      `Folder ID for ${flattenedPath} found in cache: ${cache[flattenedPath]}`,
    );
    return cache[flattenedPath];
  }

  for (const dir of pathParts) {
    currentPath += `/${dir}`;

    if (cache[currentPath]) {
      console.log(
        `Folder ID for ${currentPath} found in cache: ${cache[currentPath]}`,
      );
      parentId = cache[currentPath]; // Folder exists, get ID from cache
    } else {
      // Folder does not exist, create it and update cache
      let folderId;
      try {
        const createFolderResponse = await createFolderInStoryblok(
          dir,
          parentId,
        );
        console.log(`Created folder: ${dir} under parent ${parentId}...`);

        folderId = createFolderResponse.data.story.id;
      } catch (error) {
        const match = (error as any).response?.[0].match(
          /Slug `([^`]+)` already taken/,
        );

        if (match) {
          const [_, slug] = match;
          const story = await getFolderBySlug(slug);

          if (story) {
            folderId = story.id;
          }
        } else {
          console.log(
            `Error creating folder: ${dir} under parent ${parentId}: ${error.message}`,
          );
        }
      }

      stats.createdFolderCount++;

      cache[currentPath] = folderId;
      parentId = folderId;
    }
  }

  return parentId; // Return the final folder's ID
};

/**
 * Create a new folder in Storyblok
 */
export const createFolderInStoryblok = async (
  name: string,
  parentId: string,
): Promise<any> => {
  const slug = createSlug(name);

  options.debug &&
    console.log(`Creating folder: ${name} under parent ${parentId}...`);

  return await storyblokClient.post(
    `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
    {
      name,
      slug,
      path: `{{it.folder.slug}}/`, // Ensure the whole path is included in the slug
      parent_id: parseInt(parentId),
      is_folder: true,
    },
  );
};

/**
 * Create a new story in Storyblok
 */
export const createStoryInStoryblok = async (
  article: ArticleData,
  parentFolderId: string,
): Promise<any> => {
  const articleContent = [
    {
      component: 'dynamo_section',
      vertical_rhythm: '2xl', // Adds some spacing between header and content
      content: [
        {
          component: 'knowledge_html',
          code: {
            code: article.html,
            title: article.title,
            language: 'html',
          },
        },
      ],
    },
  ];

  options.debug &&
    console.log(
      yellow`Creating story: ${article.title} under parent ${parentFolderId}...`,
    );

  console.log(articleContent[0].content[0].code.code);

  const response = await storyblokClient.post(
    `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
    dynamicPage(
      article.title,
      article.title,
      article.slug,
      articleContent,
      parentFolderId,
    ),
  );

  return response.story;
};

/**
 * Get a story by its slug
 */
export const getStoryBySlug = async (slug: string): Promise<any> => {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/`,
      {
        starts_with: slug,
      },
    );

    return response.data?.stories.find(
      (story: any) => story.full_slug === slug, // Ensure we get the exact slug as starts_with can return multiple
    );
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
};

/**
 * Get folder by its slug
 */
export const getFolderBySlug = async (slug: string): Promise<any> => {
  options.debug && console.log(`Getting folder by slug: ${slug}...`);

  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/`,
      {
        by_slugs: slug,
        folder_only: true,
      },
    );

    const folder = response.data?.stories.find(
      (story: any) => story.full_slug === slug,
    );

    return folder;
  } catch (error) {
    console.warn(
      `Error getting folder (will try to create it): ${error.message}`,
    );
  }
};

/**
 * Handle uploading an asset to Storyblok
 */
export const uploadStoryblokAsset = async (
  asset_folder_id: string,
  filePath: string,
  targetFileName: string,
): Promise<string> => {
  if (!existsSync(filePath)) {
    throw new Error(`File does not exist: ${filePath}`);
  }

  try {
    const assetRoot = `spaces/${process.env.STORYBLOK_SPACE_ID}/assets/`;

    console.log(
      yellow`Uploading ${targetFileName} to ${assetRoot}${asset_folder_id}...`,
    );

    const createAssetPayload = {
      filename: targetFileName.replace('storyblok-export-artpage-', ''),
      asset_folder_id,
      meta_data: {
        imported_from: 'O2 Confluence',
      },
      is_private: false,
      alt: '',
    } as any;

    let size;

    // Calculate size if asset is an image
    if (targetFileName.match(/\.(png|jpe?g|gif)$/)) {
      const image = gm(filePath);
      const { width, height } = await getSize(image);
      size = `${width}x${height}`;
      createAssetPayload.size = size;
    }

    const { data: signedResponseObj } = (await storyblokClient.post(
      assetRoot,
      createAssetPayload,
    )) as any;

    let form = new FormData();

    for (let key in signedResponseObj.fields) {
      form.append(key, signedResponseObj.fields[key]);
    }

    form.append('file', readFileSync(filePath));

    const file = (await new Promise((resolve, reject) => {
      form.submit(signedResponseObj.post_url, (err, _res) => {
        if (err) {
          reject(err);
        }

        const finishPath = `spaces/${process.env.STORYBLOK_SPACE_ID}/assets/${signedResponseObj.id}/finish_upload`;

        storyblokClient.get(finishPath).then(({ data }) => {
          const finalImgUrl = `https://a.storyblok.com/${signedResponseObj.fields.key}`;
          console.log(green`${finalImgUrl} uploaded!`);
          resolve(data);
        });
      });
    })) as Promise<any>;

    return (await file).filename;
  } catch (error) {
    console.error('Error uploading asset:', error);
    throw new Error('Failed to upload asset');
  }
};

/**
 * Get all links in a given folder slug
 */
export const getAllLinksBySlug = async (slug: string): Promise<any> => {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
};

/**
 * Create a 'topic' navigation item in Storyblok
 */
export const createTopicNav = async (
  title: string,
  stories: ISbStoryData[],
  parentFolderID: string,
): Promise<any> => {
  try {
    const response = await storyblokClient.post(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories`,
      topicNavigation('Navigation', stories, [], parentFolderID, title),
    );

    stats.createdNavigationPagesCount++;

    return response;
  } catch (error: unknown) {
    throw new Error(`Error creating article nav: ${JSON.stringify(error)}`);
  }
};

/**
 * Processor to take all asset URLs in the HTML and upload them to Storyblok,
 * replacing the URLs with the new Storyblok URLs
 */
export const swapAssetsInHtml = async (
  html: string,
  dirPath: string,
  articleSlug: string,
): Promise<string> => {
  const urlRegex = /\s+(src|href)="([^"]+)"/g;
  let matches;

  console.log(`Swapping assets in ${articleSlug}...`);

  while ((matches = urlRegex.exec(html)) !== null) {
    const [fullMatch, attribute, url] = matches;

    // Some relative paths are incorrect, so we need to clean them up
    let cleanedPath = url.replace('/download/attachments/', 'attachments/');

    console.log(`Cleaning path: ${cleanedPath}`);

    if (!cleanedPath.startsWith('attachments/')) {
      continue;
    }

    if (options.dryRun) {
      options.debug && console.log(`Dry run: skipping asset upload for ${url}`);
      continue;
    }

    let modifiedUrl;

    try {
      /**
       * If the asset is an MP4 file, we need to find the actual file in the filesystem
       * as the Confluence download URL is not the actual file. The assumption is that
       * the MP4 file will be the only MP4 file in the directory.
       */
      if (url.includes('.mp4')) {
        const assetFileDir = cleanedPath.split('/').slice(0, -1).join('/');

        options.debug &&
          console.log(
            `Extrapolating MP4 filesystem location from ${assetFileDir}...`,
          );

        const findActualMp4File = await getFirstFileByExtension(
          `${dirPath}/${assetFileDir}`, // Full path to the asset file from app root
          'mp4',
        );

        if (findActualMp4File) {
          cleanedPath = `${assetFileDir}/${findActualMp4File}`;
        }
      }

      const filePath = `${dirPath}/${cleanedPath}`;
      const targetFileName = `o2_${articleSlug}_${cleanedPath.split('/').pop() || ''}`;
      const existingAsset = await getAssetByFilename(targetFileName);

      if (existingAsset) {
        console.log(blue`Skipping upload of existing asset: ${targetFileName}`);
        modifiedUrl = existingAsset.filename;
      } else {
        modifiedUrl = await uploadStoryblokAsset(
          process.env.PARENT_ASSET_FOLDER_ID!,
          filePath,
          targetFileName,
        );
      }

      stats.createdAssetCount++;
    } catch (error) {
      throw new Error(`Failed to upload asset: ${error.message}`);
    }

    html = html.replace(
      fullMatch,
      `${attribute}="${modifiedUrl.replace('s3.amazonaws.com/', '')}"`,
    );
  }

  return html;
};

/**
 * Get an asset by its slug
 */
export const getAssetByFilename = async (filename: string): Promise<any> => {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/assets/`,
      {
        search: filename,
      },
    );

    return response.data?.assets[0];
  } catch (error: unknown) {
    throw new Error(`Error getting asset: ${JSON.stringify(error)}`);
  }
};
