import { promises as fs } from 'fs';
import { JSDOM } from 'jsdom';
import { join } from 'path';
import slugify from 'slugify';
import { ArticleData, ImageSize } from './interfaces';

const navDisallowList = ['Home', 'Storyblok Export', 'NavPage-Mobile'];

/**
 * Helper function to get the size of an image
 */
export const getSize = (image: any): Promise<ImageSize> =>
  new Promise((resolve, reject) => {
    image.size((err: any, size: { width: number; height: number }) => {
      if (err) {
        reject(err);
      }
      resolve(size);
    });
  });

/**
 * Convert titles to slugs (e.g, "Hello World" -> "hello-world")
 */
export const createSlug = (title: string): string =>
  slugify(title, {
    lower: true,
    strict: true,
  });

/**
 * Remove confluence 'rater' element from the bottom of the article
 */
export const removeRaterElement = (html: string): string => {
  const regex =
    /<table[^>]*\bclass=['"][^'"]*\brater\b[^'"]*['"][^>]*>([\s\S]*?)<\/table>/gi;
  return html.replace(regex, '');
};

/**
 * Process the navigation string into an array
 */
export const processNavigation = (navigation: string): string[] =>
  navigation
    .split(',')
    .filter(Boolean)
    .filter((item) => !item.includes('Home'))
    .map((item: string) => item.trim());

/**
 * Recursively get all HTML files in a directory
 */
export const getHtmlFiles = async (dirPath: string): Promise<string[]> => {
  const files = await fs.readdir(dirPath);
  const filePaths = [];

  for (const file of files) {
    const filePath = join(dirPath, file);
    const stats = await fs.stat(filePath);

    if (stats.isFile() && filePath.endsWith('.html')) {
      console.log(`Processing HTML file: ${filePath}`);
      filePaths.push(filePath);
    } else if (stats.isDirectory()) {
      const subDirFiles = await getHtmlFiles(filePath);
      filePaths.push(...subDirFiles);
    }
  }

  return filePaths;
};

/**
 * Remove the 'ArtPage-' or 'NavPage-' prefix from the file name
 */
export const removeArticleTypePrefix = (input: string): string => {
  return input.replace(/(ArtPage-|NavPage-)/, '');
};

export const removeExportPrefix = (input: string): string => {
  return input.replace(/Storyblok Export : /, '');
};

/**
 * Extract all the required article data from the HTML
 */
export const extractArticleContent = async (
  html: string,
  filePath: string,
): Promise<ArticleData> => {
  const dom = new JSDOM(html);
  const mainContent: HTMLDivElement =
    dom.window.document.getElementById('main-content');
  const title: string = dom.window.document.title;
  const articleType = title.includes('ArtPage-') ? 'article' : 'navigation';

  const breadcrumbLinks: NodeListOf<HTMLAnchorElement> =
    dom.window.document.querySelectorAll('#breadcrumbs li a');

  /**
   * Get the article hierarchy from the breadcrumb portion of the article,
   * filtering out any 'Home' sections and empty links
   */
  const navigation = Array.from(breadcrumbLinks)
    .map((link) => link.textContent)
    .filter(Boolean)
    .filter((link) => !navDisallowList.includes(link))
    .map((link) => removeExportPrefix(removeArticleTypePrefix(link)));

  const dirPath = filePath.split('/').slice(0, -1).join('/');

  // Filter out folder suffixes
  const filteredDirPath = dirPath
    .replace(/NavPage-/gi, '')
    .replace(/ArtPage-/gi, '');

  const cleanedTitle = removeExportPrefix(removeArticleTypePrefix(title));
  const slug = createSlug(cleanedTitle);
  const fullSlug = `${navigation.map((item) => createSlug(item)).join('/')}/${slug}`;

  if (mainContent) {
    return {
      title: cleanedTitle,
      slug,
      fullSlug,
      articleType,
      html: mainContent.innerHTML,
      navigation,
      filePath,
      dirPath: filteredDirPath,
      folderName: filteredDirPath.split('/').pop() || '',
    };
  } else {
    throw new Error('No main content found');
  }
};

/**
 * Map of Confluence article categories to Storyblok folders
 */
const articleCategoryMap: { [key: string]: string } = {
  RET: 'retail',
  FRAUD: 'fraud',
};

/**
 * Convert the URL format from '/display/RET/Article+Name+Here' to 'retail/article-name-here'
 */
function convertUrlFormat(url: string): string {
  const parts = url.split('/');
  const category = articleCategoryMap[parts[0]] || createSlug(parts[0]);
  const titleSlug = createSlug(parts[1].replace(/\+/g, '-').toLowerCase());
  const newUrl = `${process.env.TARGET_BASE_URL_PATH}/${category}/${category}-${titleSlug}`;

  return newUrl;
}

/**
 * Convert hyperlinks that begin with '/display/' in the HTML to Storyblok URLs, using a known pattern
 */
export const updateHyperlinkPaths = (
  html: string,
  _dirPath: string,
  articleSlug: string,
): string => {
  return html.replace(/href="\/display\/([^"]+)"/g, (match, url) => {
    // Convert the matched URL part to the new format
    const newUrl = convertUrlFormat(url);
    // Return the updated href attribute
    return `href="${newUrl}"`;
  });
};

/**
 * Remove element with class 'smalltext' and value of '(Storyblok Export)'
 */
export const removeExportElement = (html: string): string => {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const elements = document.querySelectorAll('.smalltext');

  elements.forEach((element) => {
    if (element.textContent === '(Storyblok Export)') {
      element.remove();
    }
  });

  return dom.serialize();
};

/**
 * Remove page type prefix from all anchor links under any div with the class of 'details'
 */
export const removePageTypePrefix = (html: string): string => {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const details = document.querySelectorAll('.details');

  details.forEach((detail) => {
    const links = detail.querySelectorAll('a');

    links.forEach((link) => {
      link.textContent = removeArticleTypePrefix(link.textContent);
      link.setAttribute(
        'href',
        removeArticleTypePrefix(link.getAttribute('href')),
      );
    });
  });

  return dom.serialize();
};

/**
 * Process all hyperlinks that end in '.html', open the local file and extract the title, converting the URL to /category/title-slug
 */
export const updateLocalHyperlinks = async (
  html: string,
  dirPath: string,
  articleSlug: string,
): Promise<string> => {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const links: HTMLAnchorElement[] = Array.from(
    document.querySelectorAll('a[href]'),
  );

  for (const link of links) {
    const href = link.getAttribute('href');

    if (href && href.endsWith('.html')) {
      const file = join(dirPath, href);
      const fileContent = await fs.readFile(file, 'utf-8');
      const title = new JSDOM(fileContent).window.document.title;

      const cleanedTitle = removeExportPrefix(removeArticleTypePrefix(title));
      const slug = createSlug(cleanedTitle);

      // Normalize the URL to the new format to get picked up/processed by the 'fix links' migration script
      const newUrl = `/display/SE/${slug}`;

      console.log(
        `Updating local hyperlink: ${href} -> ${newUrl} (${cleanedTitle})`,
      );

      link.setAttribute('href', newUrl);
    }
  }

  return dom.serialize();
};

/**
 * Replace emoticon images with emojis
 */
export const replaceEmoticonsWithEmojis = (html: string): string => {
  const emoticonToEmoji: { [key: string]: string } = {
    add: '➕',
    check: '✅',
    tick: '✅',
    cross: '❌',
    error: '❌',
    information: 'ℹ️',
    warning: '⚠️',
    lightbulb_on: '💡',
    bullet_blue: '🔵',
    wait: '⏳',
  };

  // Use a regular expression to find all <img> tags with the `data-emoticon-name` attribute
  return html.replace(
    /<img[^>]*data-emoticon-name="([^"]*)"[^>]*>/g,
    (match, emoticonName) => {
      // Use the captured `emoticon name` to get the corresponding emoji
      console.log(`Replacing emoticon: ${emoticonName}`);
      const emoji = emoticonToEmoji[emoticonName] || '❓';
      return emoji;
    },
  );
};

/**
 * Strip out the inline comment spans from the HTML
 */
export const removeInlineCommentSpans = (htmlString: string): string => {
  const dom = new JSDOM(htmlString);
  const document = dom.window.document;
  const spans: HTMLSpanElement[] = document.querySelectorAll(
    '.inline-comment-marker',
  );

  spans.forEach((span) => {
    // console.log(red`Removing inline comment span: ${span.outerHTML}`);
    const docFragment = document.createDocumentFragment();
    docFragment.appendChild(document.createTextNode(span.textContent || ''));
    span.parentNode?.replaceChild(docFragment, span);
  });

  return dom.serialize();
};

/**
 * Get the first file in a directory with a given extension
 */
export const getFirstFileByExtension = async (
  dirPath: string,
  extension: string,
): Promise<string | null> => {
  const files = await fs.readdir(dirPath);
  const file = files.find((file) => file.endsWith(extension));

  console.log(`Found file with extension ${extension}: ${file}`);

  if (!file) {
    return null;
  }

  return file;
};
