import { v4 as uuidv4 } from 'uuid';

export const accordionGroupBlok = (accordions: any[]) => ({
  _uid: uuidv4(),
  component: 'dynamo_accordion_group',
  accordions,
});
