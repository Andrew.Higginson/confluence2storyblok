import { v4 as uuidv4 } from 'uuid';

export const accordionBlok = (title, body) => ({
  _uid: uuidv4(),
  component: 'dynamo_accordion',
  title,
  children: body,
  expanded: false,
  icon_new: '',
});
