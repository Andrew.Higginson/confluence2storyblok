import { v4 as uuidv4 } from 'uuid';

export type AlertVariant = 'success' | 'info' | 'warning' | 'error';

export const alertTemplate = (
  variant: string,
  title: string,
  content: any[],
) => ({
  _uid: uuidv4(),
  title,
  content,
  variant,
  component: 'dynamo_alert',
});
