import { footer } from './footer.template';
import { header } from './header.template';

export const dynamicPage = (
  title: string,
  pageTitle: string,
  slug: string,
  body: any,
  parentFolderId: string,
  theme = 'o2',
  seo = {},
) => {
  return {
    story: {
      name: title,
      slug,
      parent_id: parentFolderId,
      content: {
        component: 'dynamo_dynamic_page',
        footer: footer(),
        header: header(pageTitle),
        theme,
        seo,
        body,
      } as any,
    },
  };
};
