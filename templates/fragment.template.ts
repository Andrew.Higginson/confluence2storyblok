import { v4 as uuidv4 } from 'uuid';

export const fragmentBlok = (fragment: string) => ({
  _uid: uuidv4(),
  fragment,
  component: 'dynamo_content_fragment',
});
