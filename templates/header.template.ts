export const header = (title: string) => [
  {
    fragment: 'b503ce58-9cc9-4179-88ef-907afcf29ce2',
    component: 'dynamo_content_fragment',
  },
  {
    body: [
      {
        layout: 'full-width',
        content: [
          {
            title: title,
            content: {
              type: 'doc',
              content: [
                {
                  type: 'paragraph',
                },
              ],
            },
            subtitle: '',
            component: 'dynamo_text',
            quicklink: false,
            title_size: 'T800',
            title_color: '',
            title_weight: 'regular',
            content_color: '',
            subtitle_size: 'T600',
            subtitle_color: '',
            subtitle_weight: 'bold',
            title_alignment: 'center',
            content_alignment: '',
            subtitle_alignment: '',
            title_margin_bottom: false,
            subtitle_margin_bottom: false,
          },
        ],
        variant: 'transparent',
        component: 'dynamo_section',
        reduced_spacing: '',
        vertical_rhythm: 'md',
      },
    ],
    shape: 'curved',
    height: '',
    component: 'dynamo_rounded_header',
  },
];
