export const horizontalRule = () => [
  {
    layout: 'cozy',
    content: [
      {
        component: 'dynamo_horizontal_rule',
      },
    ],
    variant: 'primary',
    component: 'dynamo_section',
    reduced_spacing: '',
    vertical_rhythm: 'md',
  },
];
