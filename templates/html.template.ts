import { v4 as uuidv4 } from 'uuid';

interface HtmlBlok {
  _uid: string;
  code: {
    code: string;
    language: 'html';
  };
  component: 'knowledge_html';
}

export const htmlBlok = (html: string): HtmlBlok => ({
  _uid: uuidv4(),
  code: {
    code: html,
    language: 'html',
  },
  component: 'knowledge_html',
});
