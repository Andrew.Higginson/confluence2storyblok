import { v4 as uuidv4 } from 'uuid';

export const linkTemplate = (title: string, id: string) => ({
  _uid: uuidv4(),
  link: {
    id,
    linktype: 'story',
    fieldtype: 'multilink',
  },
  title,
  component: 'dynamo_link',
});
