import { v4 as uuidv4 } from 'uuid';
import { linkTemplate } from './link.template';

export const quicklinksBlokTemplate = (_links: any[]) => ({
  _uid: uuidv4(),
  links: _links.map((link: any) => linkTemplate(link.name, link.uuid)),
  component: 'dynamo_links_list',
});
