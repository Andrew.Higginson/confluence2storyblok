import { v4 as v4uuid } from 'uuid';

export const richtextBlokTemplate = (content: any[]) => ({
  _uid: v4uuid(),
  title: '',
  content,
  component: 'dynamo_text',
  quicklink: false,
  title_size: 'T800',
  title_weight: 'heavy',
  subtitle_size: 'T600',
  subtitle_weight: 'bold',
  title_margin_bottom: false,
  subtitle_margin_bottom: false,
});
