import { v4 as uuidv4 } from 'uuid';

interface SectionBlok {
  _uid: string;
  content: any[];
  component: 'dynamo_section';
  vertical_rhythm: string;
}

export const sectionBlok = (children: any[]) => ({
  _uid: uuidv4(),
  content: children,
  component: 'dynamo_section',
  vertical_rhythm: 'md',
});
