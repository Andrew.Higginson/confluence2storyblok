import { v4 as uuidv4 } from 'uuid';

export const textBlokTemplate = (title: string) => ({
  _uid: uuidv4(),
  subtitle: title,
  content: {
    type: 'doc',
    content: [
      {
        type: 'paragraph',
      },
    ],
  },
  component: 'dynamo_text',
  subtitle_size: 'T600',
  subtitle_color: 'info',
  subtitle_weight: 'regular',
});
