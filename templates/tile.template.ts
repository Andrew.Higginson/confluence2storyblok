interface TileIcon {
  name: string;
  value: string;
  iconSet: string;
  dimension_value: string;
}

export const tile = (title: string, storyId: string, icon: TileIcon) => ({
  icon,
  link: {
    id: storyId,
    linktype: 'story',
    fieldtype: 'multilink',
  },
  title,
  component: 'dynamo_tile',
  hide_title: false,
});
