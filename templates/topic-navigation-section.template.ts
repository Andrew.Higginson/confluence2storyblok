import { ISbLink } from 'storyblok-js-client';
import { tile } from './tile.template';

export const topicNavigationSection = (
  title: string,
  subtitle: string,
  stories: ISbLink[],
) => [
  {
    layout: 'cozy',
    content: [
      {
        title: '',
        content: {
          type: 'doc',
          content: [
            {
              type: 'paragraph',
            },
          ],
        },
        subtitle: title,
        component: 'dynamo_text',
        title_size: 'T900',
        title_color: '',
        title_weight: 'heavy',
        content_color: '',
        subtitle_size: 'T500',
        subtitle_color: '',
        subtitle_weight: 'bold',
        title_alignment: '',
        content_alignment: '',
        subtitle_alignment: 'center',
        title_margin_bottom: false,
        subtitle_margin_bottom: false,
      },
      {
        title: '',
        content: {
          type: 'doc',
          content: [
            {
              type: 'paragraph',
              content: [
                {
                  text: subtitle,
                  type: 'text',
                },
              ],
            },
          ],
        },
        subtitle: '',
        component: 'dynamo_text',
        title_size: 'T900',
        title_color: '',
        title_weight: 'heavy',
        content_color: '',
        subtitle_size: 'T500',
        subtitle_color: '',
        subtitle_weight: 'bold',
        title_alignment: '',
        content_alignment: 'center',
        subtitle_alignment: 'center',
        title_margin_bottom: false,
        subtitle_margin_bottom: true,
      },
    ],
    variant: 'primary',
    component: 'dynamo_section',
    reduced_spacing: '',
    vertical_rhythm: 'md',
  },
  {
    layout: 'cozy',
    content: [
      {
        body: stories.map((story) =>
          tile(
            story.name,
            story.uuid,
            story.slug === 'navigation'
              ? {
                  name: 'Grid 2 plus',
                  value:
                    'https://a.storyblok.com/f/253875/150x150/6448c18f67/grid-2-plus.svg',
                  iconSet: 'Light',
                  dimension_value:
                    'https://a.storyblok.com/f/253875/150x150/6448c18f67/grid-2-plus.svg',
                }
              : {
                  name: 'Memo circle info',
                  value:
                    'https://a.storyblok.com/f/253875/169x150/3185fbaa82/memo-circle-info.svg',
                  iconSet: 'Light',
                  dimension_value:
                    'https://a.storyblok.com/f/253875/169x150/3185fbaa82/memo-circle-info.svg',
                },
          ),
        ),

        component: 'dynamo_grid',
        columns_lg: '4',
        columns_md: '2',
        columns_sm: '2',
      },
    ],
    variant: 'primary',
    component: 'dynamo_section',
    reduced_spacing: '',
    vertical_rhythm: 'md',
  },
];
