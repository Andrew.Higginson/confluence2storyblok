import { ISbLink } from 'storyblok-js-client';
import { createSlug } from '../src/utils';
import { dynamicPage } from './dynamic-page.template';
import { horizontalRule } from './horizontal-rule.template';
import { topicNavigationSection } from './topic-navigation-section.template';

export const topicNavigation = (
  title: string,
  stories: ISbLink[],
  topics: ISbLink[],
  parentTopicFolderId: string,
  mainTitle = 'O2 Knowledge Hub',
) =>
  dynamicPage(
    `${title}`,
    `${mainTitle}`,
    `${createSlug(title)}`,
    [
      ...(stories.length
        ? topicNavigationSection(
            'Articles',
            'Click the article you need',
            stories,
          )
        : []),
      ...(topics.length ? horizontalRule() : []),
      ...(topics.length
        ? topicNavigationSection(
            'Topics',
            'Not found what you need? Choose from these options below',
            topics,
          )
        : []),
    ],
    parentTopicFolderId,
  );
