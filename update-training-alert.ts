require('dotenv').config(); // Keep this at the top
import { bold, green } from 'ansis';
import { Command } from 'commander';
import { JSDOM } from 'jsdom';
import StoryblokClient, {
  ISbLink,
  ISbStory,
  ISbStoryData,
} from 'storyblok-js-client';

const STORYBLOK_RELEASE = 142891;

const program = new Command();
program
  .name('fix-confluence-links')
  .description(
    'Find all alerts that mention training and update the text and links',
  )
  .option('-d, --dry-run', 'Run the script without actually updating any links')
  .parse(process.argv);

const options = program.opts();

const storyblokClient = new StoryblokClient({
  accessToken: process.env.STORYBLOK_ACCESS_TOKEN,
  oauthToken: process.env.STORYBLOK_OAUTH_TOKEN,
});

let filteredStories: ISbStoryData[] = [];

/**
 * Get all links in a given folder slug
 */
async function getAllStoryLinksBySlug(slug: string): Promise<ISbLink[]> {
  try {
    const response = await storyblokClient.getAll(`cdn/links`, {
      starts_with: `${slug}/`,
      is_folder: false,
      // release_id: STORYBLOK_RELEASE,
      cv: Date.now(), // Do not use cache
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting links: ${JSON.stringify(error)}`);
  }
}

async function getAllStoriesBySlug(slug: string): Promise<any> {
  try {
    const response = await storyblokClient.getAll(`cdn/stories`, {
      starts_with: `${slug}/`,
      // release_id: STORYBLOK_RELEASE,
      cv: Date.now(), // Do not use cache
    });

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting stories: ${JSON.stringify(error)}`);
  }
}

async function getStoryByID(id: string): Promise<any> {
  try {
    const response = await storyblokClient.get(
      `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${id}`,
      {
        // release_id: STORYBLOK_RELEASE,
      },
    );

    return response;
  } catch (error: unknown) {
    throw new Error(`Error getting story: ${JSON.stringify(error)}`);
  }
}

/**
 * Find all HTML elements with the class of 'confluence-information-macro' that contain a p element with the class of 'title' and text of 'Training'. Also replace for innerHTML of the div element with class 'confluence-information-macro-body' with new text and links.
 */
function updateTrainingAlerts(html: string): string {
  const dom = new JSDOM(html);
  const document = dom.window.document;
  const body = document.body;

  const alerts = Array.from(
    body.querySelectorAll('.confluence-information-macro') as HTMLDivElement[],
  ).filter((alert) => {
    const title = alert.querySelector('.title');
    return title?.innerHTML?.includes('Training');
  });

  if (alerts.length === 0) {
    return html;
  }

  alerts.forEach((alert) => {
    const title = alert.querySelector('.title');
    title.textContent = 'O2 Stellar';

    const body = alert.querySelector('.confluence-information-macro-body');
    body.innerHTML = `
      <p>To access Stellar for all your latest news and updates, <a href="https://o2stellar.com/">follow this link</a></p>
    `;
  });

  return body.innerHTML;
}

async function findAndUpdateHtmlBlok(obj: any): Promise<any> {
  function deepClone(object: any): any {
    return JSON.parse(JSON.stringify(object));
  }

  async function updateNestedObject(nestedObj: any): Promise<void> {
    for (const key in nestedObj) {
      if (typeof nestedObj[key] === 'object' && nestedObj[key] !== null) {
        updateNestedObject(nestedObj[key]);
      } else if (nestedObj.component === 'knowledge_html') {
        const html = nestedObj.code.code;
        if (html) {
          nestedObj.code.code = updateTrainingAlerts(html);
        }
      }
    }
  }

  const clonedObj = deepClone(obj);
  await updateNestedObject(clonedObj);
  return clonedObj;
}

async function updateLinks() {
  // First fetch all story references using the link API
  const stories: ISbLink[] = await getAllStoryLinksBySlug(
    process.env.STORYBLOK_BASE_SLUG,
  );

  const filteredStories = stories.filter(
    (story: ISbStoryData) =>
      story.is_folder === false && story.name !== 'Navigation',
  );

  console.log(`Found ${filteredStories.length} stories`);

  for (const [index, story] of filteredStories.entries()) {
    console.log(
      bold`Processing [${(index + 1).toString()}/${stories.length.toString()}] ${story.name}`,
    );

    const fullStory = (await getStoryByID(story.id.toString())) as ISbStory;
    const storyContent = fullStory.data.story as ISbStoryData;
    const storyBody = storyContent.content.body;

    const updatedStoryBody = await findAndUpdateHtmlBlok(storyBody);

    if (updatedStoryBody === storyBody) {
      console.log(`No changes detected for ${story.name}`);
      continue;
    }

    console.log(green`Updating ${story.name}`);

    if (!options.dryRun) {
      // Update the story with the new body
      await storyblokClient.put(
        `spaces/${process.env.STORYBLOK_SPACE_ID}/stories/${story.id}`,
        {
          ...storyContent,
          content: {
            ...storyContent.content,
            body: updatedStoryBody,
          },
          // release_id: STORYBLOK_RELEASE,
        },
      );
    }
  }
}

updateLinks();
